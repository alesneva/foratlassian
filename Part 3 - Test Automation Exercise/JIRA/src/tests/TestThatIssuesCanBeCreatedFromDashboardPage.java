package tests;

import objects.Issue;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pageObjects.DashboardPage;
import pageObjects.FiltersPage;
import pageObjects.LoginPage;

/*
The class contains tests to verify if issues can be created from Dashboard page
 */
public class TestThatIssuesCanBeCreatedFromDashboardPage extends BaseClass{

    @BeforeTest
	public void login(){
		LoginPage loginPage = new LoginPage(driver);
		loginPage.
				openPage().
				login("lesneva@gmail.com","Alya123!");
	}

    /*
    Method the tests that 'Bug' issue can be created
     */
	@Test
	public void testABugCanBeCreatedFromDashboardPage() throws Exception {

        FiltersPage filtersPage = new FiltersPage(driver);
        //open saved filter for open bugs to check the list is empty
        filtersPage.openAFilter(baseUrl + "/issues/?filter=59726").searchIssues();
        Assert.assertEquals(filtersPage.checkNoIssuesFound(), Boolean.TRUE);

        //go to Dashboard page
		DashboardPage dashboardPage = new DashboardPage(driver);
		dashboardPage.openPage().openCreateIssueDialog();

        //create issue with type 'Bug' from Dashboard page
		Issue newIssue = new Issue();
		newIssue.setIssueType("Bug");
		newIssue.setSummary("New issue with type 'Bug' has been created");
		dashboardPage.createIssue(newIssue);

        //open the filter again and assert there is 1 issue found
        filtersPage.openAFilter(baseUrl + "/issues/?filter=59726").searchIssues().waitForResults();
        Assert.assertEquals(filtersPage.checkNoIssuesFound(), Boolean.FALSE);
        Assert.assertEquals(filtersPage.getNumberOfIssues(), (Integer) (1));

        //make this is the newly created issue
        Assert.assertEquals(filtersPage.getIssueType(), "Bug");
        Assert.assertEquals(filtersPage.getIssueSummary(), "New issue with type 'Bug' has been created");

	  }

}
