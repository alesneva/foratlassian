package tests;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.WebDriver;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

/* This is a class that initiates WebDriver instance
 */
public class BaseClass{

    public WebDriver driver;
    String baseUrl = "https://jira.atlassian.com";

    @BeforeSuite
    public void setUp(){
        driver = new ChromeDriver();
    }


    @AfterSuite
    public void cleanUp(){
        driver.close();
    }
}
