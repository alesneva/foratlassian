package pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by alevtinalesneva on 16/02/2015.
 * The class represents Dashboard page
 */
public class DashboardPage extends Page {

	public DashboardPage(WebDriver driver) {
		super(driver);
	}

	String pageUrl = "https://jira.atlassian.com/secure/Dashboard.jspa";

    /**
     * Opens Dashboard page
     * @return DashboardPage object
     */
	public DashboardPage openPage(){
        driver.navigate().to(pageUrl);
		return PageFactory.initElements(driver, DashboardPage.class);
	}

}
