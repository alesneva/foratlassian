package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


/**
 * Created by alevtinalesneva on 16/02/2015.
 * The class represents Filters page
 */
public class FiltersPage extends Page {

    public FiltersPage(WebDriver driver){
        super(driver);
    }

    //button to search issues
    @FindBy(css = ".aui-button.aui-button-subtle.search-button span")
    public WebElement searchIssuesButton;

    //result table
    @FindBy(css = "#issuetable tbody")
    public WebElement issueTable;

    //results panel
    @FindBy(css = ".results-panel.navigator-item")
    public WebElement resultsPanel;


    /**
     * Opens a filter defined by given url
     * @param url the url to filter
     * @return FiltersPage object
    */
    public FiltersPage openAFilter(String url){
        driver.navigate().to(url);
        return PageFactory.initElements(driver, FiltersPage.class);
    }

    /**
     * Checks if result table is empty
     * @return true if result table is empty, false otherwise
     */
    public Boolean checkNoIssuesFound(){
        try {
            driver.findElement(By.cssSelector(".empty-results")).getTagName();
            return true;
        } catch (NoSuchElementException e) {
            return false;
            }
    }

    /**
     * Clicks on Search button to find issues
     * @return FiltersPage object
     */
    public FiltersPage searchIssues(){
        searchIssuesButton.click();
        (new WebDriverWait(driver, 10)).until(ExpectedConditions.elementToBeClickable(resultsPanel));
        return PageFactory.initElements(driver, FiltersPage.class);
    }

    public FiltersPage waitForResults(){
        searchIssuesButton.click();
        (new WebDriverWait(driver, 10)).until(ExpectedConditions.elementToBeClickable(issueTable));
        return PageFactory.initElements(driver, FiltersPage.class);
    }

    /**
     * Gets number of issues in result table
     @return number of issues from result table
     */
    public Integer getNumberOfIssues(){
        return issueTable.findElements(By.tagName("tr")).size();
    }

    /**
     * Gets summary of a first issue in the table
     * @return summary of a first issue in the table
     */
    public String getIssueSummary(){
        return issueTable.findElement(By.cssSelector("tr .summary a")).getText();
    }

    /**
     * Gets type of a first issue in the table
     * @return type of a first issue in the table
     */
    public String getIssueType() {
        return issueTable.findElement(By.cssSelector("tr .issuetype img")).getAttribute("alt");
    }
}
