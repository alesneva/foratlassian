package pageObjects;

import objects.Issue;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

/**
 * Created by alevtinalesneva on 16/02/2015.
 * The class represents main page object
 */
public class Page {
	
	public static WebDriver driver;
	
	public Page(WebDriver driver) {
		PageFactory.initElements(driver, this);
		Page.driver = driver;
	}
	
	//button to open 'create issue' dialog
	@FindBy(css = "#create_link")
	public WebElement createIssue;	

    //create issue dialog
	@FindBy(css = "#create-issue-dialog")
	public WebElement createIssueDialog;

        //issue type field
        @FindBy(css = "#issuetype-field")
        public WebElement issueType;

        //issue summary field
        @FindBy(css = "#summary")
        public WebElement issueSummary;

        //create issue button
        @FindBy(css = "#create-issue-submit")
        public WebElement createIssueButton;

    /**
     * Opens 'create issue' dialog
     */
	public void openCreateIssueDialog(){
		createIssue.click();
		(new WebDriverWait(driver, 10)).until(ExpectedConditions.elementToBeClickable(createIssueButton));
		String title = createIssueDialog.findElement(By.cssSelector(".jira-dialog-heading>h2")).getText();
		Assert.assertEquals(title, "Create Issue");
	}

    /**
     * Fills issue type and issue summary for a new issue and submit the issue
     * @param issue Issue object that represents issue to be created
     * @return Page object
     */
	public Page createIssue(Issue issue){
		issueType.click();
		issueType.sendKeys(issue.getIssueType());
		issueType.sendKeys(Keys.ENTER);	
		(new WebDriverWait(driver, 10)).until(ExpectedConditions.elementToBeClickable(issueSummary));
		issueSummary.sendKeys(issue.getSummary());
        createIssueButton.click();
		return PageFactory.initElements(driver, Page.class);
	}
	

}
