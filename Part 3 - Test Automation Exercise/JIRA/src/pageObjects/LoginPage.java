package pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by alevtinalesneva on 16/02/2015.
 * The class represents Login page
 */
public class LoginPage extends Page{

	public LoginPage(WebDriver driver) {
        super(driver);
	}
	
	String pageUrl = "https://id.atlassian.com/login";

    //username field
	@FindBy(css = "#username")
	public WebElement emailInput;

    //password field
	@FindBy(css = "#password")
	public WebElement passwordInput;

    //button to login
	@FindBy(css = "#login-submit")
	public WebElement loginButton;

    /**
     * Fills login form with given values and performs login
     * @param email user's email to login
     * @param password user's password to login
     * @return Page object
     */
	public Page login(String email, String password){
		emailInput.sendKeys(email);
		passwordInput.sendKeys(password);
		loginButton.click();
		return PageFactory.initElements(driver, DashboardPage.class);
	}

    /**
     * Open Login page
     * @return LoginPage object
     */
	public LoginPage openPage(){
        driver.navigate().to(pageUrl);
		return PageFactory.initElements(driver, LoginPage.class);
	}


}
