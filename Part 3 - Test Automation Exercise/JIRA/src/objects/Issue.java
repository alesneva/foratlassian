package objects;

/**
 * Created by alevtinalesneva on 16/02/2015.
 * The class represents Issue object
 */
public class Issue {

	String project = "";
	String[] issueTypeList = {"Bug", "Improvement", "New Feature", "Task"};
	String issueType = "";
	String summary = "";
	String assignee = "";

    /**
     * Sets issue type if it exists in the list of known issue types
     * @param givenIssueType
     */
	public void setIssueType(String givenIssueType){
		if(issueTypeList.toString().contains(givenIssueType)){
			this.issueType = givenIssueType;
		}else {
			this.issueType = "";
		}		
	}

    /**
     * Gets issue type
     * @return issue type
     */
	public String getIssueType(){
		return issueType;
	}

    /**
     * Sets given summary
     * @param givenSummary summary value
     */
	public void setSummary(String givenSummary){
		this.summary = givenSummary;
	}

    /**
     * Gets summary of issue
     * @return summary value
     */
	public String getSummary(){
		return summary;
	}

}
