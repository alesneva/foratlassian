1. Configuration.
- make sure that your chromedriver location is according to the table:
for Linux - /usr/bin/google-chrome1
for Mac - /Applications/Google\ Chrome.app/Contents/MacOS/Google\ Chrome
for Windows XP - %HOMEPATH%\Local Settings\Application Data\Google\Chrome\Application\chrome.exe
for Windows Vista - C:\Users\%USERNAME%\AppData\Local\Google\Chrome\Application\chrome.exe
Otherwise system property "webdriver.chrome.driver" must be set

2. Assumptions
- Different browsers can be tested via parameterization but Chrome browser was chosen for this test;
- It is possible to create an issue from many pages in JIRA, Dashboard page was chosen for this particular test as an example;
- It is possible to create an issue with different issue types, BUG type was chosen for this test as an example;
- The test doesn't create issues with other types because it would be difficult to identify errors if the test fails;
- Class for Issue object contains getters and setters only Issue type and Summary fields for simplification. The set of fields can be easily extended;
- Class for Page object contains menu items of header because the menu is common for other pages.
- I use a created filter to define do any 'Bug' issues exist. 
A method that uses access to database to check the same functionality would allow to do the same action faster;
- We have to be sure that the test entry conditions will always be the same: no records found by the used filter. 
possible solutions:
-- the test is executed once within a test run on a clean environment;
-- we write more code to close the newly created issue;
- A browser profile can be used to decrease time of execution if it's needed (Login action will be omitted)